package net.rfactor.chat.server.test;

import java.util.Arrays;
import java.util.concurrent.PriorityBlockingQueue;

import net.rfactor.chat.server.impl.Command;
import junit.framework.Assert;
import junit.framework.TestCase;

public class CommandTest extends TestCase {
	public void testCommandOrderInPriorityQueue() throws Exception {
	    PriorityBlockingQueue<Command> q = new PriorityBlockingQueue<Command>();
	    q.put(new Command("cmd", "chat", "server", "message1"));
	    q.put(new Command("cmd", "chat", "server", "message2"));
	    q.put(new Command("cmd", "chat", "server", "message3"));
	    q.put(new Command("cmd", "boot", "server", "driver1"));
	    q.put(new Command("cmd", "boot", "server", "driver2"));
	    q.put(new Command("cmd", "chat", "server", "message4"));
	    q.put(new Command("cmd", "chat", "server", "message5"));
		Command[] a = q.toArray(new Command[q.size()]);
		Arrays.sort(a);
		Assert.assertEquals("driver1", a[0].getArgs()[3]);
		Assert.assertEquals("driver2", a[1].getArgs()[3]);
		Assert.assertEquals("message1", a[2].getArgs()[3]);
		Assert.assertEquals("message2", a[3].getArgs()[3]);
		Assert.assertEquals("message3", a[4].getArgs()[3]);
		Assert.assertEquals("message4", a[5].getArgs()[3]);
		Assert.assertEquals("message5", a[6].getArgs()[3]);
	}
}
