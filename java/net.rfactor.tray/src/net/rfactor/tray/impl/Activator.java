package net.rfactor.tray.impl;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuComponent;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import net.rfactor.tray.MenuAction;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {
    private BundleContext m_bundleContext;
    private SystemTray m_tray;
    private TrayIcon m_trayIcon;
    private PopupMenu m_popup;
    
    private Map<MenuAction, MenuComponent> m_map = new HashMap<MenuAction, MenuComponent>();
    private String m_applicationName;

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        m_applicationName = System.getProperty("application.name", "Tray Icon");
        manager.add(createComponent()
            .setImplementation(this)
            .add(createServiceDependency()
                .setService(MenuAction.class)
                .setRequired(false)
                .setAutoConfig(false)
                .setCallbacks("addMenuItem", "removeMenuItem")
            )
        );
    }
    
    public void init() {
    	try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
			        try {
			            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			        }
			        catch (Exception e1) {
			            e1.printStackTrace();
			        }
			        m_trayIcon = null;
			        if (SystemTray.isSupported()) {
			            m_tray = SystemTray.getSystemTray();
			            // load an image
			            URL imageResource = getClass().getResource("/net/rfactor/tray/impl/icon.png");
						Image image = Toolkit.getDefaultToolkit().getImage(imageResource);
			            m_popup = new PopupMenu();
			            // create menu item for the default action
			            MenuItem defaultItem = new MenuItem("Quit");
			            defaultItem.addActionListener(new ActionListener() {
			                public void actionPerformed(ActionEvent e) {
			                    SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>() {
			                        @Override
			                        protected Void doInBackground() throws Exception {
			                            m_bundleContext.getBundle(0).stop();
			                            return null;
			                        }
			                    };
			                    sw.execute();
			                }});
			            m_popup.add(new MenuItem("-"));
			            m_popup.add(defaultItem);
			            // construct a TrayIcon
			            m_trayIcon = new TrayIcon(image, m_applicationName, m_popup);
			            m_trayIcon.setImageAutoSize(true);
			            // add the tray image
			            try {
			                m_tray.add(m_trayIcon);
			            } 
			            catch (AWTException e) {
			                System.err.println(e);
			            }
			        }
			        else {
			            // disable tray option in your application or
			            // perform other actions
			        }
				}});
		}
    	catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void addMenuItem(final MenuAction action) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				MenuItem item = new MenuItem(action.getName());
				item.addActionListener(new Listener(action));
				m_map.put(action, item);
				m_popup.insert(item, 0);
				
			}
		});
    }
    
    public void removeMenuItem(final MenuAction action) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
		        MenuComponent item = m_map.get(action);
		        m_popup.remove(item);
			}
		});
    }

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        if (m_tray != null && m_trayIcon != null) {
    		SwingUtilities.invokeLater(new Runnable() {
    			@Override
    			public void run() {
    				m_tray.remove(m_trayIcon);
    			}
    		});
        }
    }
}

class Listener implements ActionListener {
    private final MenuAction m_action;
    
    public Listener(MenuAction action) {
        m_action = action;
    }
    
    public void actionPerformed(ActionEvent e) {
        m_action.performAction();
    }
}
