package net.rfactor.trackview;

public interface TrackViewerConstants {
    public static final String TRACKVIEWER_PID = "net.rfactor.trackviewer";
    public static final String RFACTORFOLDER_KEY = "rfactorfolder";
}
