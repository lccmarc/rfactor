## Prerequisites

1. Eclipse IDE
2. Bndtools 2 plugin
3. SourceTree

## Initial Checkout

1. Point your browser at: https://bitbucket.org/marrs/rfactor
2. Select "Clone" and then "Clone in SourceTree" (and allow your browser to start SourceTree)
3. Select a destination path somewhere on disk and select "Clone"
4. Wait until SourceTree is done
5. Open Eclipse, and go or switch to a workspace folder that equals the "java" folder in the destination path above
6. Select "Import" from the menu, and choose "Existing projects into workspace"
7. Select the default folder, leave all projects it finds selected, and import them
8. Switch to Bndtools perspective, wait until Eclipse built the whole project


## Committing changes

1. You can commit changes you make, and commits will go to your local repository. You can do as many of them as you like.
2. If you want to share your commits, you "push" them to the central repository (all commits will be preserved).

## Merging incoming changes

1. You can "pull" incoming changes into your local repository. To keep things simple, make sure you have first committed any outstanding local changes.

## TODO

There's many more things to say about Git and working with it. This is the really short summary. :)
