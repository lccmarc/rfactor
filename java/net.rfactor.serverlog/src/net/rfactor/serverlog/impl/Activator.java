package net.rfactor.serverlog.impl;

import net.rfactor.serverlog.ServerLog;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {

	@Override
	public void init(BundleContext context, DependencyManager manager) throws Exception {
        manager.add(createComponent()
            .setInterface(ServerLog.class.getName(), null)
            .setImplementation(ServerLogImpl.class)
            .add(createServiceDependency().setService(EventAdmin.class).setRequired(false))
            .add(createServiceDependency()
                .setService(HttpService.class)
                .setRequired(false)
                .setAutoConfig(false)
                .setCallbacks("addHttpService", "removeHttpService")
                )
            );
	}

	@Override
	public void destroy(BundleContext context, DependencyManager manager) throws Exception {
	}
}
