package net.rfactor.livescoring.v2.impl;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Dictionary;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.ScoringData;
import net.rfactor.livescoring.VehicleData;
import net.rfactor.livescoring.WeatherData;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.log.LogService;

public class PluginListener implements Runnable, ManagedService {
    public static final String PORT = "port";
    public static final String PID = "net.rfactor.livescoring.v2";
    private volatile int m_liveScoringPort = 6789;
    private int m_currentSequence = -1;
    private Queue<ByteBuffer> m_buffers = new LinkedList<ByteBuffer>();
    private volatile boolean m_restart;
    private volatile boolean m_running;
    private Thread m_thread;
    
    private volatile EventAdmin m_eventAdmin;
    private volatile LogService m_logService;
    
    public void start() {
        m_running = true;
        m_restart = false;
        m_thread = new Thread(this, "PluginListener v2");
        m_thread.start();
    }
    
    public void stop() {
        m_running = false;
        m_thread.interrupt();
        try {
			m_thread.join(1000);
		}
        catch (InterruptedException e) {
			e.printStackTrace();
		}
    }

    public void run() {
        while (m_running) {
            DatagramSocket socket = null;
            byte[] buffer = new byte[512];
            try {
                socket = new DatagramSocket(m_liveScoringPort);
                socket.setSoTimeout(10000); // after 10 seconds, stop listening, as server might be busy or off-line
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                while (m_running && !m_restart) {
                    socket.receive(packet);
                    byte[] data = packet.getData();
                    handle(data);
                }
                m_restart = false;
            } 
            catch (SocketTimeoutException e) {
            	// socket timed out, this might not be a problem at all
            }
            catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING, "Communication data error", e);
                e.printStackTrace();
            }
            finally {
            	if (socket != null) {
            		socket.close();
            		socket = null;
            	}
            }
        }
    }
    
    private void handle(byte[] data) {
        ByteBuffer bb = ByteBuffer.allocate(data.length);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        bb.put(data);
        bb.rewind();
        byte protocolVersion = bb.get();
        byte packet = bb.get();
        short sequence = bb.getShort();
        
        //System.out.println("packet " + protocolVersion + "/" + packet + "/" + sequence);
        
        if (protocolVersion != 1) {
            m_logService.log(LogService.LOG_WARNING, "Unknown protocol version " + protocolVersion + ", parsing might fail");
        }

        // TODO add proper sequence and packet order checking
        if (m_currentSequence == -1) {
            m_currentSequence = sequence;
        }
        if (m_currentSequence != sequence) {
        }

        if (packet == 0 && !m_buffers.isEmpty()) {
            ByteBuffer dest = ByteBuffer.allocate(m_buffers.size() * 512);
            dest.order(ByteOrder.LITTLE_ENDIAN);
            byte[] tmp = new byte[512 - 4];
            ByteBuffer b = m_buffers.poll();
            while (b != null) {
                int remaining = b.remaining();
                b.get(tmp, 0, remaining);
                dest.put(tmp, 0, remaining);
                b = m_buffers.poll();
            }
            dest.rewind();
            try {
                process(dest);
            }
            catch (Exception e) {
                m_logService.log(LogService.LOG_WARNING, "Error parsing data", e);
            }

            // clear the queue
            m_buffers.clear();
        }
        // add to the queue
        m_buffers.add(bb);
    }

    /*
    
void ExampleInternalsPlugin::UpdateScoring(const ScoringInfoV2 &info) {
	StartStream();
	StreamData((char *)&type_scoring, sizeof(char));

	// session data (changes mostly with changing sessions)
	StreamString((char *)&info.mTrackName, 64);
	StreamData((char *)&info.mSession, sizeof(long));

	// event data (changes continuously)
	StreamData((char *)&info.mCurrentET, sizeof(double));
	StreamData((char *)&info.mEndET, sizeof(double));
	StreamData((char *)&info.mLapDist, sizeof(double));
	StreamData((char *)&info.mNumVehicles, sizeof(long));

	StreamData((char *)&info.mGamePhase, sizeof(byte));
	StreamData((char *)&info.mYellowFlagState, sizeof(byte));
	StreamData((char *)&info.mSectorFlag[0], sizeof(byte));
	StreamData((char *)&info.mSectorFlag[1], sizeof(byte));
	StreamData((char *)&info.mSectorFlag[2], sizeof(byte));
	StreamData((char *)&info.mStartLight, sizeof(byte));
	StreamData((char *)&info.mNumRedLights, sizeof(byte));

	// scoring data (changes with new sector times)
	for (long i = 0; i < info.mNumVehicles; i++) {
		VehicleScoringInfoV2 &vinfo = info.mVehicle[i];
		StreamData((char *)&vinfo.mPos.x, sizeof(double));
		StreamData((char *)&vinfo.mPos.z, sizeof(double));
		StreamData((char *)&vinfo.mPlace, sizeof(char));
		StreamData((char *)&vinfo.mLapDist, sizeof(double));
		StreamData((char *)&vinfo.mPathLateral, sizeof(double));
		const double metersPerSec = sqrtf( ( vinfo.mLocalVel.x * vinfo.mLocalVel.x ) +
                                      ( vinfo.mLocalVel.y * vinfo.mLocalVel.y ) +
                                      ( vinfo.mLocalVel.z * vinfo.mLocalVel.z ) );
		StreamData((char *)&metersPerSec, sizeof(double));
		StreamString((char *)&vinfo.mVehicleName, 64);
		StreamString((char *)&vinfo.mDriverName, 32);
		StreamString((char *)&vinfo.mVehicleClass, 32);
		StreamData((char *)&vinfo.mTotalLaps, sizeof(short));
		StreamData((char *)&vinfo.mBestSector1, sizeof(double));
		StreamData((char *)&vinfo.mBestSector2, sizeof(double));
		StreamData((char *)&vinfo.mBestLapTime, sizeof(double));
		StreamData((char *)&vinfo.mLastSector1, sizeof(double));
		StreamData((char *)&vinfo.mLastSector2, sizeof(double));
		StreamData((char *)&vinfo.mLastLapTime, sizeof(double));
		StreamData((char *)&vinfo.mCurSector1, sizeof(double));
		StreamData((char *)&vinfo.mCurSector2, sizeof(double));
		StreamData((char *)&vinfo.mNumPitstops, sizeof(short));
		StreamData((char *)&vinfo.mNumPenalties, sizeof(short));
		StreamData((char *)&vinfo.mInPits, sizeof(bool));
		StreamData((char *)&vinfo.mSector, sizeof(char));
		StreamData((char *)&vinfo.mFinishStatus, sizeof(char));
	}
	EndStream();
}

     */
    private void process(ByteBuffer bb) {
//        System.out.println("Processing buffer " + bb.capacity());
    	int type = bb.get();
    	if (type == 1) {
    		// telemetry data
    		// ignore for now
    		//System.out.println("ignoring telem data");
    	}
    	else if (type == 2) {
    		try {
	    		// scoring data
	    		String trackName = getString(bb, 64);
	    		int session = bb.getInt();
	    		double eventTime = bb.getDouble();
	    		double endEventTime = bb.getDouble();
	    		double trackLapDistance = bb.getDouble();
	    		int numberOfVehicles = bb.getInt();
	    		if (numberOfVehicles > 128) {
	    			// highly unlikely
	        		System.out.println("???????????");
	    			return;
	    		}
	    		int gamePhase = bb.get();
	    		int yellowFlagState = bb.get();
	    		int[] sectorFlags = new int[3];
	    		sectorFlags[0] = bb.get();
	    		sectorFlags[1] = bb.get();
	    		sectorFlags[2] = bb.get();
	    		int startLight = bb.get();
	    		int numRedLights = bb.get();

	    		// 
	    		// Get Weather data
	    		//
	    		// TODO Values currently set to 0 until the weatherdata can be read from the plugin
	    		float darkCloud = 0f;
	    		float raining = 0f;
	    		float ambientTemp = 0f;
	    		float windX = 0f;
	    		float windY = 0f;
	    		float windZ = 0f;
	    		float onPathWetness = 0f;
	    		float offPathWetness = 0f;
	    		
	    		WeatherData weatherData = new WeatherData(darkCloud, raining, ambientTemp, windX, windY, windZ, onPathWetness, offPathWetness);
	    		// End weather data
	    		
	    		VehicleData[] vehicleData = new VehicleData[numberOfVehicles];
	    		for (int i = 0; i < numberOfVehicles; i++) {
	    			double xPosition = bb.getDouble();
	    			double zPosition = bb.getDouble();
	    			int place = bb.get();
	    			double lapDistance = bb.getDouble();
	    			double pathLateral = bb.getDouble();
	    			double speed = bb.getDouble();
	    			String vehicleName = getString(bb, 64);
	    			String driverName = getString(bb, 32);
	    			String vehicleClass = getString(bb, 32);
	    			int totalLaps = bb.getShort();
	    			double bestSector1 = bb.getDouble();
	    			double bestSector2 = bb.getDouble();
	    			double bestLapTime = bb.getDouble();
	    			double lastSector1 = bb.getDouble();
	    			double lastSector2 = bb.getDouble();
	    			double lastLapTime = bb.getDouble();
	    			double currentSector1 = bb.getDouble();
	    			double currentSector2 = bb.getDouble();
	    			double timeBehindLeader = bb.getDouble();
                    int lapsBehindLeader = bb.getInt();
                    double timeBehindNext = bb.getDouble();
                    int lapsBehindNext = bb.getInt();
	    			int numberOfPitstops = bb.getShort();
	    			int numberOfPenalties = bb.getShort();
	    			boolean inPits = bb.get() != 0;
	    			int sector = bb.get();
	    			int finishStatus = bb.get();
	    			vehicleData[i] = new VehicleData(xPosition, zPosition, place, lapDistance, pathLateral, speed, 
	    					vehicleName, driverName, vehicleClass, totalLaps, bestSector1, bestSector2, bestLapTime, 
	    					lastSector1, lastSector2, lastLapTime, currentSector1, currentSector2, timeBehindLeader, lapsBehindLeader, timeBehindNext, lapsBehindNext, numberOfPitstops, 
	    					numberOfPenalties, inPits, sector, finishStatus);
	    		}
	    		String resultStreamData = getVarString(bb);
	    		ScoringData sd = new ScoringData(trackName, session, trackLapDistance, eventTime, endEventTime, gamePhase, yellowFlagState, sectorFlags, startLight, numRedLights, numberOfVehicles, vehicleData, resultStreamData, weatherData);
	    		sendData(sd);
    		}
    		catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    public void sendData(ScoringData data) {
        Properties props = new Properties();
        props.put(LiveScoringConstants.SCORINGDATA, data);
        props.put(LiveScoringConstants.SCORINGSERVER, "server-" + m_liveScoringPort);
        Event event = new Event(LiveScoringConstants.TOPIC, (Dictionary) props);
        m_eventAdmin.postEvent(event);
    }
    
    public static String getString(ByteBuffer bb, int length) {
        byte[] name = new byte[length];
        for (int i = 0; i < name.length; i++) {
            byte b = bb.get();
            if (b == 0) {
                String string = new String(name, 0, i);
                return string;
            }
            else {
                name[i] = b;
            }
        }
        String string = new String(name);
        return string;
    }
    
    public String getVarString(ByteBuffer bb) {
        int length = bb.getInt();
        if (length < 1 || length > 8000) {
        	if (length != 0) {
        		m_logService.log(LogService.LOG_WARNING, "String too long or short (" + length + "), returning empty string");
        	}
            return "";
        }
        byte[] name = new byte[length];
        for (int i = 0; i < name.length; i++) {
            byte b = bb.get();
            if (b == 0) {
                return new String(name, 0, i);
            }
            else {
                name[i] = b;
            }
        }
        return new String(name);
    }

    public void updated(Dictionary props) throws ConfigurationException {
        if (props != null) {
            m_liveScoringPort = Integer.parseInt((String) props.get(PORT));
            m_restart = true;
        }
    }
}
