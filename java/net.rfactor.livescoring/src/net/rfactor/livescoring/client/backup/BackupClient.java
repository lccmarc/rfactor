package net.rfactor.livescoring.client.backup;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.ScoringData;
import net.rfactor.livescoring.VehicleData;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class BackupClient extends HttpServlet implements EventHandler {
	private static final long serialVersionUID = -4080778685979911857L;
	private static final String HTTP_ENDPOINT = "/backup-status";
	private static final TrackPositionComparator TRACK_POSITION_COMPARATOR = new TrackPositionComparator();
	private static int LAP_DELTA = 1; // add one lap to everybody's lap
	private static long TIME_DELTA = 10 * 60; // add 10 minutes to the race time to allow for the time to restart
	private ScoringData m_oldData = null;
	private String m_leader = "";
	private int m_laps = 0;
	private String m_latestGrid = "";
	private String m_latestLaps = "";
	private int m_currentSession = 0;
	
	public void start() {
		System.out.println("Race standings backup active.");
		System.out.println("For a status overview, point your browser at http://localhost:8080" + HTTP_ENDPOINT);
	}
	
	@Override
	public void handleEvent(Event event) {
		ScoringData data = (ScoringData) event.getProperty(LiveScoringConstants.SCORINGDATA);
		if (data != null) {
			int session = data.getSession();
			if (session > 9) {
				if (m_oldData != null) {
					if (data.getEventTime() < m_oldData.getEndEventTime()) {
						// we've moved to a different session, time to reset
						m_leader = "";
						m_laps = 0;
						m_latestGrid = "";
						m_latestLaps = "";
					}
					VehicleData[] vehicles = data.getVehicleScoringData();
					for (int i = 0; i < vehicles.length; i++) {
						VehicleData vehicleData = vehicles[i];
						if (vehicleData.getPlace() == 1) {
							m_leader = vehicleData.getDriverName() + " in the " + vehicleData.getVehicleName();
							m_laps = vehicleData.getTotalLaps();
							VehicleData oldVehicleData;
							VehicleData[] oldVehicles = m_oldData.getVehicleScoringData();
							try {
								oldVehicleData = oldVehicles[i];
							}
							catch (IndexOutOfBoundsException e) {
								// weird, the car was not there in the previous event
								// yet suddenly it's the leading car
								continue;
							}
							
							if (vehicleData.getLapDistance() < oldVehicleData.getLapDistance()) {
								if (vehicleData.getSector() == 1 && oldVehicleData.getSector() == 0) {
									System.out.println("Backing up standings...");
									StringBuffer grid = new StringBuffer();
									StringBuffer laps = new StringBuffer();
									// advance race time
									laps.append("/forwardseconds " + (TIME_DELTA + Math.round(m_oldData.getEventTime())) + "\n");
									// set grid for restart
									Arrays.sort(oldVehicles, TRACK_POSITION_COMPARATOR);
									int pos = 1;
									for (VehicleData v : oldVehicles) {
										grid.append("/editgrid " + (pos++) + " V:" + v.getVehicleName() + "\n");
									}
									// set laps driven
									for (VehicleData v : oldVehicles) {
										laps.append("/changelaps " + (LAP_DELTA + v.getTotalLaps()) + " V:" + v.getVehicleName() + "\n");
									}
									m_latestLaps = laps.toString();
									m_latestGrid = grid.toString();
									System.out.println("Grid:\n" + m_latestGrid);
									System.out.println("Laps:\n" + m_latestLaps);
									try {
										save("backup-lap-" + m_laps + "-grid", m_latestGrid);
										save("backup-lap-" + m_laps + "-laps", m_latestLaps);
									}
									catch (Exception e) {
										System.out.println("Could not save laps and grid:");
										e.printStackTrace();
									}
								}
								else {
									// most likely the session ended
								}
							}
						}
					}
				}
				
				m_oldData = data;
			}
			// not in a race session
			if (m_currentSession != session) {
				if (session <= 9) {
					System.out.println("Not in a race session.");
				}
				else {
					System.out.println("In a race session.");
				}
				m_currentSession = session;
			}
		}
	}
	
	private void save(String filename, String contents) throws Exception {
		FileWriter fw = new FileWriter(filename);
		fw.write(contents);
		fw.close();
	}
	
	@SuppressWarnings("unused")
	private void addHttpService(HttpService httpService) {
		try {
			httpService.registerServlet(HTTP_ENDPOINT, this, null, null);
		}
		catch (ServletException | NamespaceException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void removeHttpService(HttpService httpService) {
		httpService.unregister(HTTP_ENDPOINT);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/plain");
		PrintWriter writer = resp.getWriter();
		if (m_oldData == null) {
			writer.println("No data received from dedicated server.");
			writer.println("Check if it is running, in a session and if the plugin is installed.");
		}
		else {
			writer.println("Track         : " + m_oldData.getTrackName());
			writer.println("Cars          : " + m_oldData.getNumberOfVehicles());
			writer.println("Leader        : " + m_leader);
			writer.println("Laps completed: " + m_laps);
			writer.println("Latest grid   :\n" + m_latestGrid);
			writer.println("Latest laps   :\n" + m_latestLaps);
		}
	}
	
	static class TrackPositionComparator implements Comparator<VehicleData> {
	    public int compare(VehicleData v1, VehicleData v2) {
	        return v2.getLapDistance() < v1.getLapDistance() ? -1 : 1;
	    }
	}
}
