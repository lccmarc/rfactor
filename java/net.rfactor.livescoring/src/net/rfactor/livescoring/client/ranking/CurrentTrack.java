package net.rfactor.livescoring.client.ranking;

public class CurrentTrack {
    enum RaceMode {
        LAPS, TIME;
    }

    private RaceMode m_raceMode;
    private double m_lapDistance;
    private String m_name;
    private int m_laps;
    private double m_time;
    private double m_startTime;
    
    public CurrentTrack(String name, int laps, double d) {
        m_name = name;
        m_laps = laps;
        m_lapDistance = d;
        m_raceMode = RaceMode.LAPS;
    }
    
    public CurrentTrack(String name, double time, double lapDistance) {
        m_name = name;
        m_time = time;
        m_lapDistance = lapDistance;
        m_raceMode = RaceMode.TIME;
    }
    
    public double getLapDistance() {
        return m_lapDistance;
    }
    
    public String getName() {
        return m_name;
    }
    
    public int getLaps() {
        return m_laps;
    }
    
    public boolean isWinner(CurrentVehicle vehicle) {
        switch (m_raceMode) {
            case LAPS:
                return vehicle.getLapsCompleted() >= m_laps;
            case TIME:
                return (vehicle.getRank() == 1 && vehicle.getLap() >= (m_startTime + m_time));
        }
        return false;
    }

    public void setStartTime(double et) {
        m_startTime = et;
    }
    
    public double getStartTime() {
        return m_startTime;
    }
}
