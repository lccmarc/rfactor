package net.rfactor.livescoring.client.ranking;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;

import com.google.gson.annotations.SerializedName;

public class Driver implements Serializable {
	private static final long serialVersionUID = -6041480967432128126L;

	@SerializedName("name")
	private String m_name;
	
	@SerializedName("cars")
	private SortedMap<String, Car> m_cars = new TreeMap<String, Car>();

	@SerializedName("lastSeenOnline")
	private Date m_lastSeenOnline;

	public Car getCar(String vehicleName) {
		return m_cars.get(vehicleName);
	}

	public void putCar(String vehicleName, Car car) {
		m_cars.put(vehicleName, car);
	}

	public void setName(String driver) {
		m_name = driver;
	}

	public Car getFastestCar() {
		try {
			String key = m_cars.firstKey();
			if (key == null) {
				return null;
			}
			return m_cars.get(key);
		}
		catch (NoSuchElementException e) {
			return null;
		}
	}
	
	public Collection<Car> getCars() {
		return m_cars.values();
	}

	public String getName() {
		return m_name;
	}
	
	public Date getLastSeenOnline() {
		return m_lastSeenOnline;
	}
	
	public boolean isOnline() {
		Date now = new Date();
		return ((now.getTime() - m_lastSeenOnline.getTime()) < 5000);
	}

	public void setLastSeenOnline() {
		m_lastSeenOnline = new Date();
	}
}
