package net.rfactor.livescoring.client.ranking;

import net.rfactor.livescoring.VehicleData;

public class CurrentVehicle {
    private double m_startedAt;
    private double m_finishedAt;
    private VehicleData m_data;
    private int m_lapsCompleted;
    private double m_sector1;
    private double m_sector2;
    private double m_lap;
    private double m_xGarage, m_zGarage;
    private boolean m_garageFound;
    private int m_rank;
    
    public void setData(VehicleData data, double et, CurrentTrack track) {
        m_data = data;
    }
    
    public void setRank(int rank) {
        m_rank = rank;
    }
    public int getRank() {
        return m_rank;
    }

    public double getSector1() {
        return m_sector1;
    }
    
    public double getSector2() {
        return m_sector2;
    }
    
    public double getLap() {
        return m_lap;
    }

    public void setLapsCompleted(int lapsCompleted) {
        m_lapsCompleted = lapsCompleted;
    }

    public int getLapsCompleted() {
        return m_lapsCompleted;
    }

    public double getCurrentLapDistance() {
        return m_data.getLapDistance();
    }
    
    @Override
    public String toString() {
        return m_data.getVehicleName();
    }

    public void setFinishedAt(double finishedAt) {
        m_finishedAt = finishedAt;
    }

    public double getFinishedAt() {
        return m_finishedAt;
    }

    public String getDriver() {
        return m_data.getDriverName();
    }
}
