package net.rfactor.livescoring.client.ranking;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.rfactor.livescoring.LiveScoringConstants;
import net.rfactor.livescoring.ScoringData;
import net.rfactor.livescoring.VehicleData;
import net.rfactor.livescoring.client.ranking.CurrentTrack.RaceMode;
import net.rfactor.livescoring.v2.impl.PluginListener;
import net.rfactor.util.TimeUtil;

import org.osgi.framework.BundleContext;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * A client that keeps track of the fastest laps that people have set on a given track on the server. The
 * data is persisted over sessions and can be hand edited if necessary.
 */
public class RankingClient extends HttpServlet implements EventHandler {
    private static final long serialVersionUID = 1L;
    private static final String ENDPOINT = "/hotlaps";
    private volatile BundleContext m_context;
    private CurrentTrack m_currentTrack;
    private Map<String, CurrentVehicle> m_currentVehicles = new HashMap<String, CurrentVehicle>();
    private double m_lastEt = -1f;
    private CurrentTrack.RaceMode m_mode = RaceMode.LAPS;
    private int m_laps = 9;
    private double m_time = 300f;
    private DateFormat m_format = DateFormat.getDateTimeInstance();
    
    private Map<String, Track> m_tracks = new HashMap<String, Track>();

    public void start() {
    	loadFromFile();    		
    }

    public void stop() {
    	saveToFile();
    }
    
	private void loadFromFile() {
		File f = getDataFile();
    	if (f.exists()) {
    		try {
	    		ObjectInputStream br = new ObjectInputStream(new FileInputStream(f));
	    		StringBuffer json = new StringBuffer();
	    		m_tracks = (Map<String, Track>) br.readObject();
	    		br.close();
	    		return;
    		}
    		catch (Exception e) {
    			e.printStackTrace();
    		}
    	}
		m_tracks = new HashMap<String, Track>();
	}
    
	private void saveToFile() {
		ObjectOutputStream bw = null;
		try {
	    	File f = getDataFile();
			bw = new ObjectOutputStream(new FileOutputStream(f));
			bw.writeObject(m_tracks);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if (bw != null) {
				try {
					bw.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
				bw = null;
			}
		}
	}
    
    private File getDataFile() {
    	return new File("hotlaps.json");
//    	return m_context.getDataFile("hotlaps.json");
    }
    
    public void handleEvent(Event event) {
        ScoringData scoringData = (ScoringData) event.getProperty(LiveScoringConstants.SCORINGDATA);
        if (scoringData != null) {
            double et = scoringData.getEventTime();
            if (m_lastEt < 0) {
                m_lastEt = et;
            }
            if (m_lastEt > et) {
                resetSession();
            }
            m_lastEt = et;
            int sessionID = scoringData.getSession();
            String session = "session=-" + sessionID; // TODO map the IDs to proper names
            if (m_currentTrack == null) {
                switch (m_mode) {
                    case LAPS:
                        m_currentTrack = new CurrentTrack(scoringData.getTrackName(), m_laps, scoringData.getLapDistance());
                        break;
                    case TIME:
                        m_currentTrack = new CurrentTrack(scoringData.getTrackName(), m_time, scoringData.getLapDistance());
                        break;
                }
            }
            Track track = m_tracks.get(m_currentTrack.getName());
            if (track == null) {
            	track = new Track();
            	track.setName(m_currentTrack.getName());
            	m_tracks.put(m_currentTrack.getName(), track);
            	// TODO initialize
            }
            
            VehicleData[] vehicleData = scoringData.getVehicleScoringData();
            boolean dataChanged = false;
            for (VehicleData data : vehicleData) {
                String name = data.getVehicleName();
                CurrentVehicle vehicle = m_currentVehicles.get(name);
                if (vehicle == null) {
                    vehicle = new CurrentVehicle();
                    m_currentVehicles.put(name, vehicle);
                }
                vehicle.setData(data, et, m_currentTrack);

                // update our persistent data
                Driver driver = track.getDriver(vehicle.getDriver());
                if (driver == null) {
                	driver = new Driver();
                	driver.setName(vehicle.getDriver());
                	track.putDriver(vehicle.getDriver(), driver);
                }
                driver.setLastSeenOnline();
                Car car = driver.getCar(data.getVehicleName());
                if (car == null) {
                	car = new Car();
                	car.setNameAndClass(data.getVehicleName(), data.getVehicleClass());
                	driver.putCar(data.getVehicleName(), car);
                }
				dataChanged |= car.setFastestLap(data.getBestSector1(), data.getBestSector2(), data.getBestLapTime(), session);
				VehicleData lastData = car.getLastData();
				if (!data.isInPits() && lastData != null && !lastData.isInPits()) {
			        if (lastData.getSector() == 0 && data.getSector() == 1) {
			            // crossed s/f
			            if (car.inHotLap()) {
			                car.addLap();
			                dataChanged = true;
			            }
			            else {
			                car.setInHotlap(true);
			            }
			        }
				}
				else {
				    if (car.inHotLap()) {
				        car.setInHotlap(false);
				    }
				}
				car.setLastData(data);

            }
            if (dataChanged) {
            	saveToFile();
            }
        }
    }
    
    private void resetSession() {
        // new session, reset
        m_currentVehicles.clear();
        m_currentTrack = null;
    }
	
    public void addHttpService(HttpService http) {
        try {
            http.registerServlet(ENDPOINT, this, null, null);
        }
        catch (ServletException e) {
            e.printStackTrace();
        }
        catch (NamespaceException e) {
            e.printStackTrace();
        }
    }

    public void removeHttpService(HttpService http) {
        http.unregister(ENDPOINT);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	//Gson gson = new Gson();
    	String path = req.getPathInfo();
		resp.setHeader("Access-Control-Allow-Origin", "*");
    	if (path == null) {
	    	Gson gson = new GsonBuilder().setPrettyPrinting().create(); // pretty printing
	    	PrintWriter writer = resp.getWriter();
	    	resp.setContentType("application/json");
	    	writer.write(gson.toJson(m_tracks));
    	}
    	else if (path.equals("/html")) {
    		PrintWriter writer = resp.getWriter();
    		resp.setHeader("Content-Security-Policy", "*");
    		resp.setContentType("text/html");
    		writer.write("<html>");
    		writer.write("<head><meta http-equiv=\"refresh\" content=\"5\"/><title>Hot Laps</title>");
	        writer.write("<style>");
    		writer.write("body { font-family: sans-serif; } " +
    				"table { border: solid 2px #000; } " +
    				"td { margin: 0px; background-color: #eee; text-align: center; font-size: 10pt; padding: 3px; } " +
    		"th { color: #fff; text-align: center; background-color: #e40; font-size: 11pt; padding: 5px; } "); 
    		writer.write("</style>");
	        writer.write("</head>");
    		writer.write("<body>");
    		if (m_currentTrack != null) {
	    		writer.write("<h1>" + m_currentTrack.getName() + "</h1>");
				writer.write("<table>");
	    		writer.write("<tr><th>Name</th><th>Car</th><th>Class</th><th>Laps</th><th>Sector 1</th><th>Sector 2</th><th>Sector 3</th><th>Laptime</th><th>Date</th><th>Online</th></tr>");
	    		Track t = m_tracks.get(m_currentTrack.getName());
	    		List<Driver> drivers = t.getDrivers();
	    		Collections.sort(drivers, new DriverComparator());
	    		for (Driver d : t.getDrivers()) {
	    			for (Car c : d.getCars()) {
		    			Date lapTimeDate = c.getLapTimeDate();
		        		writer.write("<tr>" +
		    				"<td>" + d.getName() + "</td>" +
							"<td>" + c.getName() + "</td>" +
							"<td>" + c.getVehicleClass() + "</td>" +
							"<td>" + c.getLapsDriven() + "</td>" +
							"<td>" + TimeUtil.toLapTime(c.getFastestSector1()) + "</td>" +
							"<td>" + TimeUtil.toLapTime(c.getFastestSector2()) + "</td>" +
							"<td>" + TimeUtil.toLapTime(c.getFastestSector3()) + "</td>" +
							"<td>" + TimeUtil.toLapTime(c.getFastestLapTime()) + "</td>" +
							"<td>" + (lapTimeDate == null ? "" : m_format.format(lapTimeDate)) + "</td>" +
							"<td>" + (d.isOnline() ? "Yes" : "No") + "</td>" +
							"</tr>"
						);
	    			}
	    		}
	    		writer.write("</table>");
    		}
    		else {
    			writer.write("<h1>No track loaded</h1>");
    		}
    		writer.write("</body></html>");
    	}
    	else if (path.equals("/csv")) {
	    	PrintWriter writer = resp.getWriter();
	    	resp.setContentType("text/plain");
    		writer.write("Name;Car;Class;Laps;Sector 1;Sector 2;Sector 3;Laptime;Date;Online");
    		Track t = m_tracks.get(m_currentTrack.getName());
    		List<Driver> drivers = t.getDrivers();
    		Collections.sort(drivers, new DriverComparator());
    		for (Driver d : t.getDrivers()) {
    			for (Car c : d.getCars()) {
    				Date lapTimeDate = c.getLapTimeDate();
    				writer.write(
						d.getName() + ";" +
						c.getName() + ";" +
						c.getVehicleClass() + ";" +
						c.getLapsDriven() + ";" +
						TimeUtil.toLapTime(c.getFastestSector1()) + ";" +
						TimeUtil.toLapTime(c.getFastestSector2()) + ";" +
						TimeUtil.toLapTime(c.getFastestSector3()) + ";" +
						TimeUtil.toLapTime(c.getFastestLapTime()) + ";" +
						(lapTimeDate == null ? "" : m_format.format(lapTimeDate)) + ";" +
						(d.isOnline() ? "Yes" : "No")
					);
    			}
    		}
    	}
    }
}
