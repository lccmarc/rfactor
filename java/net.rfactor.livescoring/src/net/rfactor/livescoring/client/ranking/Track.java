package net.rfactor.livescoring.client.ranking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import com.google.gson.annotations.SerializedName;

public class Track implements Serializable {
	private static final long serialVersionUID = -5638567788701974092L;

	@SerializedName("name")
	private String m_name;

	@SerializedName("drivers")
	private SortedMap<String, Driver> m_drivers = new TreeMap<String, Driver>();

	public Driver getDriver(String driver) {
		return m_drivers.get(driver);
	}

	public void putDriver(String name, Driver driver) {
		m_drivers.put(name, driver);
	}

	public void setName(String name) {
		m_name = name;
	}

	public List<Driver> getDrivers() {
		ArrayList<Driver> drivers = new ArrayList<Driver>(m_drivers.values());
		Collections.sort(drivers, new DriverComparator());
		return drivers;
	}
}
