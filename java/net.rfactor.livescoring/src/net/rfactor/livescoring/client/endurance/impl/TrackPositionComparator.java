package net.rfactor.livescoring.client.endurance.impl;

import java.util.Comparator;

import net.rfactor.livescoring.client.endurance.Vehicle;

/**
 * Compares the positions of the cars on the track (in whatever lap they are). Track positions can
 * be used to determine things like the order during the formation lap (and comparing it to the
 * qualification results).
 */
public class TrackPositionComparator implements Comparator<Vehicle> {
    public int compare(Vehicle o1, Vehicle o2) {
        return o2.getCurrentLapDistance() < o1.getCurrentLapDistance() ? -1 : 1;
    }
}