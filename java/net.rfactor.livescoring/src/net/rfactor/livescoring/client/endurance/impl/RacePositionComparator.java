package net.rfactor.livescoring.client.endurance.impl;

import java.util.Comparator;

import net.rfactor.livescoring.client.endurance.Vehicle;

/**
 * Compares the positions of the cars during a race. It takes into account the
 * status of both cars, the number of laps they've driven and their position
 * on the current lap. Before the start of the race, positions are measured
 * as the distance in the lap as if everybody is driving up to the line (so
 * the car that is closest to the line, but has not passed it, is ranked as
 * the number one).
 */
public class RacePositionComparator implements Comparator<Vehicle> {
    private final double m_trackDistance;
    
    public RacePositionComparator(double trackDistance) {
        m_trackDistance = trackDistance;
    }
    
    public int compare(Vehicle o1, Vehicle o2) {
        double d1 = 0f, d2 = 0f;
        if (o1.isRacing()) {
            d1 = o1.getLapsCompleted() * m_trackDistance + o1.getCurrentLapDistance();
        }
        else if (!o1.isFinished()) {
            d1 = -m_trackDistance + o1.getCurrentLapDistance();
        }
        if (o2.isRacing()) {
            d2 = o2.getLapsCompleted() * m_trackDistance + o2.getCurrentLapDistance();
        }
        else if (!o2.isFinished()) {
            d2 = -m_trackDistance + o2.getCurrentLapDistance();
        }
        
        if (o1.isFinished()) {
            if (o2.isFinished()) {
                int l1 = o1.getLapsCompleted();
                int l2 = o2.getLapsCompleted();
                if (l1 == l2) {
                    return o1.getFinishedAt() < o2.getFinishedAt() ? -1 : 1;
                }
                else {
                    return l2 < l1 ? -1 : 1;
                }
            }
            return -1;
        }
        else {
            if (o2.isFinished()) {
                return 1;
            }
            return d2 < d1 ? -1 : 1;
        }
    }
}