package net.rfactor.livescoring.client.endurance.impl;

import java.util.Comparator;
import net.rfactor.livescoring.client.endurance.Penalty;;

/**
 * Compare the time when the penalty was handed out
 * newest first
 */
public class PenaltyTimeComparator implements Comparator<Penalty> {
    public int compare(Penalty o1, Penalty o2) {
        return o2.getTime() < o1.getTime() ? -1 : 1;
    }
}
