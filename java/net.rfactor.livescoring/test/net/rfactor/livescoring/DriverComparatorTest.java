package net.rfactor.livescoring;

import java.util.Arrays;

import net.rfactor.livescoring.client.ranking.Car;
import net.rfactor.livescoring.client.ranking.Driver;
import net.rfactor.livescoring.client.ranking.DriverComparator;

import org.junit.Assert;
import org.junit.Test;

public class DriverComparatorTest {
	private Car c1 = new Car() {{ setFastestLap(5, 10, 15, "race"); }};
	private Car c2 = new Car() {{ setFastestLap(5, 11, 16, "race"); }};
	private Car c3 = new Car() {{ setFastestLap(6, 12, 17, "race"); }};

	private Car c4 = new Car() {{ setFastestLap(0.01, 0.02, 0.03, "race"); }};
	private Car c5 = new Car() {{ setFastestLap(0.01, 0.02, 0.04, "race"); }};
	
	
	private Driver d1 = new Driver() {{ setName("a"); }};
	private Driver d2 = new Driver() {{ setName("b"); }};
	private Driver d3 = new Driver() {{ setName("c"); }};
	
	private Driver d4 = new Driver() {{ setName("d"); putCar("c1", c1); }};
	private Driver d5 = new Driver() {{ setName("e"); putCar("c2", c2); }};
	private Driver d6 = new Driver() {{ setName("f"); putCar("c3", c3); }};

	private Driver d7 = new Driver() {{ setName("g"); putCar("c4", c4); }};
	private Driver d8 = new Driver() {{ setName("h"); putCar("c5", c5); }};
	@Test
	public void testSortEmptyList() {
		Driver[] list = new Driver[] {};
		Arrays.sort(list, new DriverComparator());
	}

	@Test
	public void testSortSingleItemList() {
		Driver[] list = new Driver[] { d1 };
		Arrays.sort(list, new DriverComparator());
	}

	@Test
	public void testSortListNoTimes() {
		Driver[] list = new Driver[] { d3, d2, d1 };
		Arrays.sort(list, new DriverComparator());
		Assert.assertArrayEquals(new Driver[] { d1, d2, d3 }, list);
	}
	
	@Test
	public void testSortList() {
		Driver[] list = new Driver[] { d6, d5, d4 };
		Arrays.sort(list, new DriverComparator());
		Assert.assertArrayEquals(new Driver[] { d4, d5, d6 }, list);
	}
	
	@Test
	public void testSortListOfZeroTimes() {
		Driver[] list = new Driver[] { d7, d8 };
		Arrays.sort(list, new DriverComparator());
		Assert.assertArrayEquals(new Driver[] { d7, d8 }, list);
	}

	@Test
	public void testSortListOfMixedTimes() {
		Driver[] list = new Driver[] { d8, d7, d6, d5, d4, d3, d2, d1 };
		Arrays.sort(list, new DriverComparator());
		Assert.assertArrayEquals(new Driver[] { d4, d5, d6, d1, d2, d3, d7, d8 }, list);
	}
}
