package net.rfactor.util;

import java.text.DecimalFormat;

public class TimeUtil {
	/** Converts a time expressed as a number of seconds into a (lap)time with a resolution of a 1000th of a second. */
	public static String toLapTime(double time) {
		return toLapTime(time, 3);
	}

	/** Converts a time expressed as a number of seconds into a (lap)time with a resolution of a 1000th of a second. */
	public static String toLapTime(double time, int decimals) {
		decimals = (decimals < 1 ? 1 : (decimals > 3 ? 3 : decimals));
		boolean isNegative = (time < 0);
		if (isNegative) {
			time *= -1.0;
		}
		double sec = Math.round(time * 1000.0) / 1000.0;
		int s = (int) sec;
		int t = (int) (1000.0 * (sec - (double) s));

		int hours = s / 3600;
		int minutes = s / 60 - hours * 60;
		int seconds = s - minutes * 60 - hours * 3600;
		StringBuffer result = new StringBuffer();
		boolean isFirstDigitBlock = true;
		if (isNegative) {
			result.append('-');
		}
		if (hours > 0) {
			result.append(format(hours, 2, false).trim());
			isFirstDigitBlock = false;
		}
		if (minutes > 0 || !isFirstDigitBlock) {
			if (!isFirstDigitBlock) {
				result.append(':');
			}
			result.append(format(minutes, 2, !isFirstDigitBlock).trim());
			isFirstDigitBlock = false;
		}
		if (!isFirstDigitBlock) {
			result.append(':');
		}
		result.append(format(seconds, 2, !isFirstDigitBlock).trim() + "." + format(t, decimals, true));
		return result.toString();
	}
	
	/** Returns a number as a string with an exact number of digits. */
	public static String format(int number, int digits, boolean padWithZeros) {
		StringBuffer result = new StringBuffer();
		result.append(number);
		int diff = digits - result.length();
		while (diff-- > 0) {
			if (padWithZeros) {
				result.insert(0, '0');
			}
			else {
				result.insert(0, ' ');
			}
		}
		return result.toString().substring(0, digits);
	}
	
	public static String formatNumber(double num, int decimals) {
		String formatString = "0.0000000000";
 		decimals = (decimals > 10 ? 10 : (decimals < 1 ? 1 : decimals)) + 2;
		DecimalFormat formatter1 = new DecimalFormat(formatString.substring(0, decimals));		
		return formatter1.format(num);
	}
	
	public static double toKmh(double meterPerSecond) {
		return (meterPerSecond * 3600.0) / 1000.0;
	}
	
    public static double toMeterPerSecond(double kilometerPerHour) {
    	return (kilometerPerHour / 3600.0) * 1000.0;
    }
	
	public static double toMinutes(double seconds) {
		return seconds / 60.0;
	}
	
	public static double toHours(double seconds) {
		return seconds / 60.0 / 60.0;
	}
	
	/** Calculates the time a car was at position p based on the time it was last measured right before and right after crossing p. */
	public static double calculateTimeAtPoint(double lastEventTime, double lastPosition, double currentEventTime, double currentPosition, double position) {
	    if (position < currentPosition && position > lastPosition && currentPosition > lastPosition) {
	        return (currentEventTime - (currentEventTime - lastEventTime) * ((currentPosition - position) / (currentPosition - lastPosition)));
	    }
	    else {
	        return currentEventTime;
	    }
	    
	}
}
