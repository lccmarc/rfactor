package net.rfactor.broadcast.director;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class DirectorImpl implements EventHandler {
	private int m_tvpos = 26;
	@Override
	public void handleEvent(Event event) {
		StringBuilder props = new StringBuilder();
		String[] names = event.getPropertyNames();
		for (String key : names) {
			if (props.length() > 0) {
				props.append(",");
			}
			props.append(key + "=" + event.getProperty(key));
		}
		System.out.println("Director Event: " + event + " " + props.toString());
		String[] split = event.getTopic().split("/");
		String type = split[1];
		if ("TeamGarageFound".equals(type)) {
			int pos = (Integer) event.getProperty("rfpos");
			String vehicle = (String) event.getProperty("vehicle");
			int delta = m_tvpos - pos;
			if (delta > 0) {
				httpGet("http://localhost:8081/broadcast/keys?keys=" + enc("{NUMPADENTER}{NUMPADADD " + delta + "}"));
			}
			else if (delta < 0) {
				httpGet("http://localhost:8081/broadcast/keys?keys=" + enc("{NUMPADENTER}{NUMPADSUB " + (-delta) + "}"));
			}
			httpGet("http://localhost:8081/broadcast/talk?msg=" + enc("The number " + vehicle.substring(1) + " is leaving the garage now."));
		}
		else {
			int pos = (Integer) event.getProperty("rfpos");
			String vehicle = (String) event.getProperty("vehicle");
			int delta = m_tvpos - pos;
			if (delta > 0) {
				httpGet("http://localhost:8081/broadcast/keys?keys=" + enc("{NUMPADENTER}{NUMPADADD " + delta + "}"));
			}
			else if (delta < 0) {
				httpGet("http://localhost:8081/broadcast/keys?keys=" + enc("{NUMPADENTER}{NUMPADSUB " + (-delta) + "}"));
			}
			httpGet("http://localhost:8081/broadcast/talk?msg=" + enc("Team number " + vehicle.substring(1) + ", event type " + type));
		}
	}
	
	private String enc(String text) {
		try {
			return URLEncoder.encode(text, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private void httpGet(String location) {
		System.out.println(location);
		try {
			URL url = new URL(location);
	        URLConnection yc = url.openConnection();
	        BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) {
	            System.out.println(inputLine);
	        }
	        in.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
