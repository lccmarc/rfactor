package net.rfactor.sync.util;

/**
 * Tuple of a file name and its hash.
 */
public class HashedFile {
	private final String m_file;
	private final String m_sha;

	public HashedFile(String file, String sha) {
		m_file = file;
		m_sha = sha;
	}
	
	public String getFile() {
		return m_file;
	}
	
	public String getSha() {
		return m_sha;
	}
	
	@Override
	public boolean equals(Object object) {
		HashedFile sf = (HashedFile) object;
		return m_file.equals(sf.m_file) && m_sha.equals(sf.m_sha);
	}
	
	@Override
	public int hashCode() {
		return m_file.hashCode() & m_sha.hashCode();
	}
	
	@Override
	public String toString() {
		return m_sha + " : " + m_file;
	}
}