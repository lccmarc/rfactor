package net.rfactor.sync.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Indexer will scan a hierarchy of folders, creating an index of all its files.
 */
public class Indexer {
	private File m_baseDir;
	private Config m_config;

	Indexer(File baseDir, File config) {
		m_baseDir = baseDir;
		if (config != null && config.isFile()) {
			try {
				load(new FileInputStream(config));
				return;
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		m_config = new Config();
	}
	
	public void load(InputStream is) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(is);
			m_config = (Config) ois.readObject();
		}
		finally {
			is.close();
		}
	}
	
	public void save(OutputStream os) throws IOException {
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(os);
			oos.writeObject(m_config);
			oos.flush();
		}
		finally {
			os.close();
		}
	}

	public File lookup(String sha) {
		String file = m_config.lookup(sha);
		return file == null ? null : new File(m_baseDir, file);
	}

	public static Indexer createSyncIndex(File baseDir, File config) throws Exception {
		long t1 = System.currentTimeMillis();
		Indexer si = new Indexer(baseDir, config);
		Config oldConfig = si.getConfig();
		si.reset();
		si.index(baseDir, baseDir, oldConfig);
		System.out.println("Indexed " + baseDir + " in " + (System.currentTimeMillis() - t1) + " ms.");
		return si;
	}

	public Config getConfig() {
		return m_config;
	}
	
	public void reset() {
		m_config = new Config();
	}

	private void index(File baseDir, File directory, Config config) throws Exception {
		File[] files = directory.listFiles();
		int index = baseDir.getPath().length() + 1;
		for (File f : files) {
			if (f.isFile()) {
				String file = f.getPath().substring(index).replace('\\', '/');
				FileData data = config.get(file);
				if (data == null || !data.equals(f)) {
					data = new FileData(f);
				}
				m_config.put(file, data);
			}
		}
		for (File f : files) {
			if (f.isDirectory()) {
				index(baseDir, f, config);
			}
		}
	}

	public Set<String> hashes() {
		return m_config.hashes();
	}
	
	public static class Config implements Serializable {
		private Map<String, FileData> m_data = new HashMap<String, FileData>();
		private Map<String, String> m_files = new HashMap<String, String>();
		
		public void put(String name, FileData data) {
			m_files.put(data.getHash(), name);
			m_data.put(name, data);
		}
		public FileData get(String name) {
			return m_data.get(name);
		}
		public String lookup(String hash) {
			return m_files.get(hash);
		}
		public Set<String> hashes() {
			return m_files.keySet();
		}
	}
	
	public static class FileData implements Serializable {
		private String hash;
		private long lastModified;
		private long size;
		
		public FileData(File file) throws Exception {
			hash = IOUtil.calculateSHA(new FileInputStream(file));
			lastModified = file.lastModified();
			size = file.length();
		}
		
		public FileData(String hash, long lastModified, long size) {
			this.hash = hash;
			this.lastModified = lastModified;
			this.size = size;
		}
		
		public String getHash() {
			return hash;
		}

		public void setHash(String hash) {
			this.hash = hash;
		}

		public long getLastModified() {
			return lastModified;
		}

		public void setLastModified(long lastModified) {
			this.lastModified = lastModified;
		}

		public long getSize() {
			return size;
		}

		public void setSize(long size) {
			this.size = size;
		}
		
		public boolean equals(File file) {
			return file != null && file.lastModified() == getLastModified() && file.length() == getSize();
		}
	}

	public void addFile(String sha, File file) throws Exception {
		m_config.put(sha, new FileData(file));
		
	}
}