package net.rfactor.sync.server;

import java.util.Properties;

import net.rfactor.sync.Index;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpService;

public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		Properties props = new Properties();
		props.put(CommandProcessor.COMMAND_SCOPE, "syncserver");
		props.put(CommandProcessor.COMMAND_FUNCTION, new String[] {"reload", "quit"});

		dm.add(createComponent()
			.setInterface(Object.class.getName(), props)
			.setImplementation(SyncServlet.class)
			.add(createServiceDependency().setService(Index.class).setRequired(true))
			.add(createServiceDependency().setService(HttpService.class).setCallbacks("add", "remove"))
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {
	}
}
