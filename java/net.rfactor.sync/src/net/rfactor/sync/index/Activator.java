package net.rfactor.sync.index;

import java.util.Properties;

import net.rfactor.sync.Index;

import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.service.command.CommandProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.service.log.LogService;

public class Activator extends DependencyActivatorBase {
	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		Properties props = new Properties();
		props.put(CommandProcessor.COMMAND_SCOPE, "syncindex");
		props.put(CommandProcessor.COMMAND_FUNCTION, new String[] {"rescan"});
		
		dm.add(createComponent()
			.setInterface(Index.class.getName(), props)
			.setImplementation(SyncIndex.class)
            .add(createServiceDependency().setService(LogService.class).setRequired(false))
		);
	}

	@Override
	public void destroy(BundleContext bc, DependencyManager dm) throws Exception {
	}
}
