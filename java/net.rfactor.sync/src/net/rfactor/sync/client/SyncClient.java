package net.rfactor.sync.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import net.rfactor.sync.Index;
import net.rfactor.sync.util.IOUtil;
import net.rfactor.sync.util.Progress;
import net.rfactor.sync.util.SyncConfig;
import net.rfactor.sync.util.SyncOps;
import net.rfactor.sync.util.SystemScanner;

import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogReaderService;
import org.osgi.service.log.LogService;

@Path("client")
public class SyncClient {
	private volatile BundleContext m_context;
	private volatile Index m_index;
    private volatile LogService m_log;
    private volatile LogReaderService m_logReader;
	private SyncConfig m_sync;
	private String m_data;
	private String m_path;
	private String m_trash;
	private String m_download;
	private Thread m_thread;
    private Progress m_progress;
	
	@GET @Path("status") @Produces("text/plain")
	public String status(@Context HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		return "Online";
	}

	@GET @Path("reload") @Produces("text/plain")
	public String startReload(@Context HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		return reload();
	}

	@GET @Path("download") @Produces("text/plain")
	public String startDownload(@Context HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		synchronized (this) {
			if (m_thread == null || !m_thread.isAlive()) {
			    m_progress = new Progress("Download");
				m_thread = new Thread(new Runnable() {

						@Override
						public void run() {
						    try {
						        download(m_progress);
						    }
						    finally {
						        m_thread = null;
						    }
						}
					}, "Download Thread");
				m_thread.start();
				m_log.log(LogService.LOG_INFO, "Starting download.");
				return "Download started.";
			}
			else {
                m_log.log(LogService.LOG_INFO, "User tried to start download. " + m_progress.getTask() + " still in progress. Ignoring.");
				return m_progress.getTask() + " in progress.";
			}
		}
	}

	@GET @Path("status") @Produces("text/plain")
	public String statusDownload(@Context HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		synchronized (this) {
			if (m_thread == null) {
				return "Idle.";
			}
			else {
				if (m_thread.isAlive()) {
					return String.format(m_progress.getTask() + ": %4.3f %%", m_progress.getProgress());
				}
				else {
					return "Done.";
				}
			}
		}
	}
	
	@GET @Path("log") @Produces("text/plain")
	public String logDownload(@Context HttpServletResponse resp) {
		resp.setHeader("Access-Control-Allow-Origin", "*");
		Enumeration log = m_logReader.getLog();
		StringBuilder builder = new StringBuilder();
		SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
		while (log.hasMoreElements()) {
		    LogEntry entry = (LogEntry) log.nextElement();
		    builder.append(f.format(new Date(entry.getTime())) + " " + entry.getLevel() + " " + entry.getMessage() + "\n");
		}
		return builder.toString();
	}
	
	@GET @Path("scan") @Produces("text/plain")
	public String scan(@Context HttpServletResponse resp) {
       resp.setHeader("Access-Control-Allow-Origin", "*");
        synchronized (this) {
            if (m_thread == null || !m_thread.isAlive()) {
                m_progress = new Progress("Scan");
                m_thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                List<File> folders = SystemScanner.findRFactorFolders(m_progress);
                                StringBuffer paths = new StringBuffer();
                                for (File folder : folders) {
                                    if (paths.length() > 0) {
                                        paths.append(',');
                                    }
                                    paths.append(folder.getAbsolutePath().replace('\\', '/'));
                                }
                                File indexProperties = new File("index.properties");
                                Properties p = new Properties();
                                try {
                                    p.load(new FileInputStream(indexProperties));
                                }
                                catch (Exception e) {
                                    p.setProperty("data", "data");
                                }
                                p.setProperty("paths", paths.toString());
                                SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                String now = f.format(new Date());

                                p.store(new FileOutputStream(indexProperties), "Written by system scan at " + now);
                                
                            }
                            catch (Exception e) {
                                m_log.log(LogService.LOG_ERROR, "Exception while scanning system.", e);
                            }
                            finally {
                                m_thread = null;
                            }
                        }
                    }, "Scan Thread");
                m_thread.start();
                m_log.log(LogService.LOG_INFO, "Starting system scan.");
                return "Scan started.";
            }
            else {
                m_log.log(LogService.LOG_INFO, "User tried to start system scan. " + m_progress.getTask() + " still in progress. Ignoring.");
                return m_progress.getTask() + " in progress.";
            }
        }
	}

	public void start() {
		reload();
	}
	
	public String reload() {
		try {
		    m_log.log(LogService.LOG_INFO, "Loading client configuration.");
			Properties p = new Properties();
			File clientProperties = new File("client.properties");
			if (!clientProperties.isFile()) {
	            m_log.log(LogService.LOG_ERROR, "Could not locate configuration file: " + clientProperties.getAbsolutePath());
				return "Error";
			}
			p.load(new FileInputStream(clientProperties));
            String data = p.getProperty("data");
			String server = p.getProperty("server");
			String path = p.getProperty("path");
			String trash = p.getProperty("trash");
			String download = p.getProperty("download");
			if (data != null && server != null && path != null && trash != null && download != null) {
				URL url = new URL(server);
				m_sync = new SyncConfig();
				m_sync.read(url.openStream());
				m_data = data;
				m_path = path;
				m_trash = trash;
				m_download = download;
			}
            m_log.log(LogService.LOG_INFO, "Done loading client configuration.");
			return "Ok";
		}
		catch (Exception e) {
            m_log.log(LogService.LOG_ERROR, "Error loading client configuration.", e);
			return "Error: " + e.getMessage();
		}
	}
	
	public void download(Progress progress) {
		try {
			if (m_sync != null && m_path != null) {
				File path = new File(m_path);
				File trash = new File(m_trash);
				File data = new File(m_data);
				if (!path.isDirectory()) {
		            m_log.log(LogService.LOG_ERROR, "Supplied 'path' setting is not an existing directory.");
					return;
				}
				if (!trash.isDirectory()) {
		            m_log.log(LogService.LOG_ERROR, "Supplied 'trash' setting is not an existing directory.");
					return;
				}
                if (!data.isDirectory()) {
                    m_log.log(LogService.LOG_ERROR, "Supplied 'data' setting is not an existing directory.");
                    return;
                }
                File config = new File(data, IOUtil.calculateSHA(new FileInputStream(new File("client.properties"))) + ".index");
				SyncOps so = m_sync.check(path, trash, m_index, m_download, config, progress);
				m_sync.getIndexer().save(new FileOutputStream(config));
			}
		}
		catch (Exception e) {
            m_log.log(LogService.LOG_INFO, "Error downloading.", e);
		}
	}
	
	public void quit() throws BundleException {
		m_context.getBundle(0).stop();
	}
}
