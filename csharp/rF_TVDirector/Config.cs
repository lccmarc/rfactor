﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace rF_TVDirector
{
    static class Config
    {
        /// <summary>
        /// Defines the time to wait between to SendKey events
        /// </summary>
        internal static int TimeBetweenSendKey = 30;

        /// <summary>
        /// Defines the time to wait after the game window has been activated
        /// </summary>
        internal static int TimeAfterWindowActivation = 100;

        /// <summary>
        /// Defines the portnumber to use for the internal webserver
        /// </summary>
        internal static int WebserverPort = 81;

        internal static bool RemoteAccess = true;

        internal static string IniFilename = Path.Combine(
                Path.GetDirectoryName(Application.ExecutablePath), 
                Path.GetFileNameWithoutExtension(Application.ExecutablePath)) + ".ini";


        public static void ReadIni()
        {
            SimSync.Utils.Ini ini = new SimSync.Utils.Ini(IniFilename,false);
            
            if (!ini.FileExists){
                WriteIni();
                return;
            }

            TimeBetweenSendKey = ini.GetInt("main", "TimeBetweenSendKey");
            if (TimeBetweenSendKey < 10 || TimeBetweenSendKey > 1000)
            {
                TimeBetweenSendKey = 50;
            }

            TimeAfterWindowActivation = ini.GetInt("main", "TimeAfterWindowActivation");
            if (TimeAfterWindowActivation < 100 || TimeAfterWindowActivation > 5000)
            {
                TimeBetweenSendKey = 500;
            }

            WebserverPort = ini.GetInt("main", "WebserverPort");
            if (WebserverPort < 81 || WebserverPort > 65535)
            {
                WebserverPort = 81;
            }

            if (ini.KeyExists("main", "RemoteAccess"))
            {
                RemoteAccess = ini.GetBool("main", "RemoteAccess");
            }
            else
            {
                ini.Write("main", "RemoteAccess", RemoteAccess);
                ini.SaveToDisk();
            }
        }

        public static void WriteIni()
        {
            SimSync.Utils.Ini ini = new SimSync.Utils.Ini(IniFilename, false);

            ini.Write("main", "TimeBetweenSendKey", TimeBetweenSendKey);
            ini.Write("main", "TimeAfterWindowActivation", TimeAfterWindowActivation);
            ini.Write("main", "WebserverPort", WebserverPort);
            ini.Write("main", "RemoteAccess", RemoteAccess);

            ini.SaveToDisk();
        }
    }
}
