﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimSync.Utils
{
    /// <summary>
    /// Create a New INI file to store or load data
    /// </summary>
    public class Ini
    {
        #region VARS
        private string _Filename = "";
        public bool IsStream = false;
        public bool FileExists = false;
        private SortedDictionary<string, string> dict = new SortedDictionary<string, string>();
        #endregion VARS

        #region PROPERTIES
        /// <summary>
        /// Get the filename of the ini
        /// </summary>
        public string Filename
        {
            get { return this._Filename; }
            set
            {
                if (File.Exists(value))
                {
                    this._Filename = value;
                    this.FileExists = true;
                }
                else
                {
                    this._Filename = "";
                    this.FileExists = false;
                }
            }
        }
        #endregion PROPERTIES

        #region CONSTRUCTOR
        public Ini()
        {
            this.Init("", false);
        }

        public Ini(string INIPath)
        {
            this.Init(INIPath, false);
        }
        /// <summary>
        /// INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath"></PARAM>
        public Ini(string INIPath, bool FromStream)
        {
            this.Init(INIPath, FromStream);
        }
        #endregion CONSTRUCTOR

        #region PUBLIC METHODS
        /// <summary>
        /// Check if a given Key exists in the ini
        /// </summary>
        /// <param name="Section">Section to search</param>
        /// <param name="Key">Key to search</param>
        /// <returns>true on success</returns>
        public bool KeyExists(string Section, string Key)
        {
            string sKey = (Section.Trim().Length > 0 ? "[" + Section.ToUpperInvariant().Trim() + "]" : "") + "_" + Key.ToUpperInvariant().Trim();
            return this.dict.ContainsKey(sKey);
        }

        /// <summary>
        /// Get the double value from the given secion / key
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public double GetDouble(string Section, string Key)
        {
            string sValue = GetString(Section, Key);
            if (sValue.Length > 0)
            {
                sValue = sValue.Replace(".", ",");
                string[] sTmp = sValue.Split(',');
                if (sTmp.Length < 2) sTmp = new String[] { sTmp[0], "0" };
                sTmp[1] += "000";
                sTmp[1] = sTmp[1].Substring(0, 3);
                double dDouble = Double.Parse(sTmp[0]) + (Double.Parse(sTmp[1]) / 1000);
                return dDouble;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get the int value from the given secion / key
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public int GetInt(string Section, string Key)
        {
            string sValue = GetString(Section, Key);
            if (sValue.Length > 0)
            {
                if (sValue.Contains("."))
                {
                    sValue = sValue.Split('.')[0];
                }
                else if (sValue.Contains(","))
                {
                    sValue = sValue.Split(',')[0];
                }
                return int.Parse(sValue);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get the boolean value from the given secion / key
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool GetBool(string Section, string Key)
        {
            string sValue = GetString(Section, Key);
            try
            {
                if (sValue.Length > 0)
                {
                    if (sValue.ToLowerInvariant().Trim().StartsWith("y")
                        || sValue.ToLowerInvariant().Trim().StartsWith("on"))
                    {
                        return true;
                    }
                }
            } catch { }
            return false;
        }

        /// <summary>
        /// Get the Sync Date value from the given secion / key
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public DateTime GetSyncDate(string Section, string Key)
        {
            try
            {
                string sTmp = GetString(Section, Key);
                string[] sDate = sTmp.Split('|');
                return new DateTime(int.Parse(sDate[0]), int.Parse(sDate[1]), int.Parse(sDate[2]), 
                    int.Parse(sDate[3]), int.Parse(sDate[4]), int.Parse(sDate[5]), DateTimeKind.Utc);
            }
            catch
            {
                return new DateTime(0);
            }
        }

        /// <summary>
        /// Get the value from the given secion / key as a List<string>
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public List<string> GetList(string Section, string Key, char Separator, bool LowerCase)
        {
            string sValue = GetString(Section, Key);
            List<string> listReturn = new List<string>();
            if (sValue.Length > 0)
            {
                if (LowerCase) sValue = sValue.ToLowerInvariant();
                if (sValue.Length > 0)
                {
                    string[] sValues = sValue.Split(Separator);
                    foreach (string sVal in sValues)
                    {
                        listReturn.Add(sVal.Trim());
                    }
                }
            }
            return listReturn;
        }


        public string GetString(string Section, string Key)
        {
            return this.Get(Section, Key);
        }

        public string Get(string Section, string Key)
        {
            string sKey = (Section.Trim().Length > 0 ? "[" + Section.ToUpperInvariant().Trim() + "]" : "") + "_" + Key.ToUpperInvariant().Trim();
            return this.dict.ContainsKey(sKey) ? this.dict[sKey] : "";
        }

        public void Write(string Section, string Key, int Value)
        {
            this.Set(Section, Key, Value.ToString());
        }
        public void Write(string Section, string Key, double Value)
        {
            this.Set(Section, Key, Value.ToString());
        }
        public void Write(string Section, string Key, bool Value)
        {
            this.Set(Section, Key, (Value ? "yes" : "no"));
        }

        public void Write(string Section, string Key, string Value)
        {
            this.Set(Section, Key, Value);
        }

        public void Set(string Section, string Key, string Value)
        {
            string sCurValue = GetString(Section, Key);
            string sKey = (Section.Length > 0 ? "[" + Section.ToUpperInvariant().Trim() + "]" : "") + "_" + Key.ToUpperInvariant().Trim();

            if (sCurValue != Value)
            {
                if (this.dict.ContainsKey(sKey))
                {
                    this.dict[sKey] = Value.Trim();
                }
                else
                {
                    try
                    {
                        this.dict.Add(sKey, Value.Trim());
                    }
                    catch { }
                }


            }
        }

        public void SaveToDisk()
        {
            // Write Ini-File
            string sContents = "";
            string sSection = "";
            string LastSection = "";
            string sKey = "";
            string sValue = "";
            int iCnt = 0;
            foreach (KeyValuePair<string, string> pair in this.dict)
            {
                iCnt++;
                if (pair.Key.StartsWith("["))
                {
                    string[] tmp = pair.Key.Split(new char[] { ']', '_' }, 2);
                    if (tmp.Length < 2) tmp = new String[] { tmp[0], tmp[1] };
                    if (tmp[0].StartsWith("[")) tmp[0] = tmp[0].Substring(1).Trim();
                    if (tmp[1].StartsWith("_")) tmp[1] = tmp[1].Substring(1).Trim();
                    LastSection = tmp[0].ToUpperInvariant();
                    sKey = tmp[1].ToUpperInvariant();
                    sValue = pair.Value;
                }
                else
                {
                    string[] tmp = { "", "" };
                    if (pair.Key.StartsWith("_")) tmp[1] = pair.Key.Substring(1).Trim();
                    LastSection = tmp[0].ToUpperInvariant();
                    sKey = tmp[1].ToUpperInvariant();
                    sValue = pair.Value;
                }
                if (LastSection != sSection)
                {
                    sSection = LastSection;
                    if (iCnt > 1) sContents += "\r\n";
                    sContents += (sSection.Length > 0 ? "[" + sSection + "]" : "") + "\r\n";
                }
                sContents += sKey + "=" + sValue + "\r\n";
            }
            try
            {
                StreamWriter fINI = new StreamWriter(this._Filename, false);
                fINI.Write(sContents);
                fINI.Close();
            }
            catch
            {
                //
            }
        }
        #endregion PUBLIC METHODS

        #region PRIVATE METHODS
        private void Init(string INIPath, bool FromStream)
        {
            this._Filename = "";
            this.FileExists = false;
            this.IsStream = FromStream;
            if (!this.IsStream && INIPath.Trim().Length == 0)
            {
                return;
            }
            string[] lines = { "" };
            if (this.IsStream)
            {
                lines = INIPath.Split('\n');
            }
            else
            {
                this._Filename = INIPath;
                if (System.IO.File.Exists(this._Filename))
                {
                    this.FileExists = true;
                    this.dict.Clear();
                    lines = System.IO.File.ReadAllLines(this._Filename);
                }
            }
            if (lines.Length > 0)
            {
                string sSection = "";
                foreach (string line in lines)
                {
                    if (line.StartsWith("'") || line.StartsWith("/"))
                    {
                        continue;
                    }
                    else if (line.StartsWith("["))
                    {
                        sSection = line.ToUpperInvariant().Trim();
                    } 
                    else if (line.Contains("="))
                    {
                        string[] tmp = line.Split(new char[] { '=' }, 2);
                        try
                        {
                            this.dict.Add(sSection + "_" + tmp[0].ToUpperInvariant().Trim(), tmp[1].Trim());
                        }
                        catch { }
                    }
                }
            }
        }
        #endregion PRIVATE METHODS
    }
}
