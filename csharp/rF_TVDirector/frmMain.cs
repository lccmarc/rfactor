﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;

namespace rF_TVDirector
{

    public partial class frmMain : Form
    {
        /*
        HTTPD.HttpServer webserverExt = null;
        Thread webserverThreadExt = null;
         */
        HTTPD.HttpServer webserverLoc = null;
        Thread webserverThreadLoc = null;

        private string LastError = "";
        private DateTime PositionTimer = new DateTime(0);
        public static frmMain frm = null;
        public static TVDirector tv = new TVDirector();
        private RemoteAccess remote = null;
        public delegate void SetPositionsDelegate(int ownPos, int newPos);
        private IPAddress server_ip = IPAddress.Loopback;

        private string AppTitle()
        {
            string result = this.Tag.ToString();
            string ver = "";
            Version v = new Version(Application.ProductVersion);
            if (v.Revision > 0)
            {
                ver = v.ToString();
            }
            else if (v.Build > 0)
            {
                ver = v.Major + "." + v.Minor + "." + v.Build;
            }
            else
            {
                ver = v.Major + "." + v.Minor;
            }
            result += " v" + ver;
            return result;
        }

        public frmMain()
        {
            frm = this;
            InitializeComponent();
            tv.TimeBetweenSendKey = Config.TimeBetweenSendKey;
            tv.TimeAfterWindowActivation = Config.TimeAfterWindowActivation;
            lblPort.Text = Config.WebserverPort.ToString();
            remote = new RemoteAccess(Config.RemoteAccess);
            txtRemoteAccess.Text = "Remote access disabled in rF_TVDirector.ini";
            if (Config.RemoteAccess)
            {
                txtRemoteAccess.Text = remote.FullKey;
            }
            this.Text = AppTitle();
            StartWebserverThread();
        }

        private void StartWebserverThread(){
            //webserverLoc = new HTTPD.HttpServer(IPAddress.Loopback, Config.WebserverPort, remote);
            webserverLoc = new HTTPD.HttpServer(IPAddress.Any, Config.WebserverPort, remote);
            webserverThreadLoc = new Thread(new ThreadStart(webserverLoc.listen));
            webserverThreadLoc.Start();

            /*
            if (Config.RemoteAccess)
            {
                webserverExt = new HTTPD.HttpServer(remote.PublicIP, Config.WebserverPort, remote);
                webserverThreadExt = new Thread(new ThreadStart(webserverExt.listen));
                webserverThreadExt.Start();
            }
            */
        }

        private void KillWebserverThread()
        {
            if (webserverThreadLoc != null)
            {
                webserverLoc.Destruct();
                try { webserverThreadLoc.Abort(); }
                catch { }
                webserverThreadLoc = null;
            }
            if (webserverLoc != null)
            {
                webserverLoc = null;
            }
            /*
            if (webserverThreadExt != null)
            {
                webserverExt.Destruct();
                try { webserverThreadExt.Abort(); }
                catch { }
                webserverThreadExt = null;
            }
            if (webserverExt != null)
            {
                webserverExt = null;
            }
             */
        }

        public void SetErrorMessage(string msg)
        {
            LastError = msg;
        }

        private void timerPos_Tick(object sender, EventArgs e)
        {
            lblOwnPos.Text = tv.OwnPosition.ToString();
            lblNewPos.Text = tv.NewPosition.ToString();
            lblErrors.Text = LastError;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            KillWebserverThread();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            Thread.Sleep(500);
            if (!webserverLoc.IsActive)
            {
                MessageBox.Show("Unable to open socket!\r\nLooks like the choosen port " + webserverLoc.Port + " is used by another application!\r\nClosing now...", AppTitle(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                Application.Exit();
                return;
            }
            /*
            if (Config.RemoteAccess)
            {
                if (!webserverExt.IsActive)
                {
                    MessageBox.Show("Remote access enabled, but the program was unable to open socket!"+
                        "\r\nCheck if the firewall is enabled or if other programs are using the same external ports!"+
                        "\r\nClosing now...", AppTitle(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    Application.Exit();
                    return;
                }
            }
             */
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                mynotifyicon.Visible = true;
                //mynotifyicon.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                mynotifyicon.Visible = false;
            }

        }

        private void mnuShow_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void mnuClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
